Dependencies:

-ROS version = indigo
-JVM version = 1.7.0_79
-Maven version = 3.0.5

-You need to have the sharpeye15 code - at leat seagull_autopilot node

To run:

- Make sure the Piccolo Command Center is turned on
- Initialize roscore
- Setup variables with the following command: 
$ . home/administrator/sharpeye15/sharpeye15_ws/devel/setup.bash 
where you should replace the path to your system path
- Run the seagull_autopilot node with the following command
$ rosrun seagull_autopilot autopilot_driver _ap_conn_type:=1 _ap_conn_type:=true _ap_host:=172.21.4.202 _ap_port:=2001 _ap_payloadstream:=false &
- Start the visualizer with the following command:
$ mvn jetty:run
