/**
 * Mapa e componentes do mapa
 */

//GLOBALS
var map;
var reachLayer;
var imageLayer;
var lokingMap = false;
var WindWestUser = 0;
var WindSouthUser = 0;
var startFlighttimeInMs;
var knotsToMS = 0.514444;
var fuelType = 0;
var fuelTypeUser = false;

//DEFINITIONS
var epsg4326;
var projectTo; 

// conection parameters
//var ip = "172.21.2.162";
//var ip = "192.168.106.95";
var ip = "localhost";

var port = "8080";


function MapInit() 
{
	// default -e atualizado manulamente pelo operador
		startFlighttimeInMs =  Date.now();

	// create the map	
	    map = new OpenLayers.Map("map");
	    map.addLayer(new OpenLayers.Layer.OSM());
	
	//Create the Reach Area Layer and add it to the map
		reachLayerSensor = new OpenLayers.Layer.Vector("Sensor area");
		map.addLayer(reachLayerSensor);	    
		
	//Create the Reach Area Layer and add it to the map
		reachLayerAPI = new OpenLayers.Layer.Vector("Forecast API area");
		map.addLayer(reachLayerAPI);	 
		
	//Create the Reach Area Layer and add it to the map
		reachLayerUser = new OpenLayers.Layer.Vector("User input area");
		map.addLayer(reachLayerUser);	 
		
	//Create the airplane Layer and add it to the map
		imageLayer = new OpenLayers.Layer.Vector("Airplane");
		map.addLayer(imageLayer);

	// create layer switcher widget in top right corner of map.
	    var layer_switcher= new OpenLayers.Control.LayerSwitcher({});
	    map.addControl(layer_switcher);

	// Definitions
	    epsg4326 =  new OpenLayers.Projection("EPSG:4326"); //WGS 1984 projection
	    projectTo = map.getProjectionObject(); //The map projection (Spherical Mercator)

	//TODO isto talvez possa sair daqui 
	//Set start center point and zoom    
	    var lonLat = new OpenLayers.LonLat(-9.3460, 38.8280).transform(epsg4326, projectTo);
	    var zoom=10;
	    map.setCenter (lonLat, zoom);
	
    //initialize the cookie
	    //setCookie();
		
	//Define the callback functions for the Json data passed    	    	
		$.getJSON("http://" + ip + ":" + port + "/uavWeb/state/airplane", setAirplane);
		$.getJSON("http://" + ip + ":" + port + "/uavWeb/state/reachAreaSensor", setReachAreaSensor);
		$.getJSON("http://" + ip + ":" + port + "/uavWeb/state/reachAreaAPI", setReachAreaAPI);
		$.getJSON("http://" + ip + ":" + port + "/uavWeb/state/reachAreaUser", setReachAreaUser);
		$.getJSON("http://" + ip + ":" + port + "/uavWeb/state/airplane", centerMapOnPlane);
		
	//Set the interval at which the functions should be called
		setInterval( function() { $.getJSON("http://" + ip + ":" + port + "/uavWeb/state/airplane", setAirplane); } , 900);
		setInterval( function() { $.getJSON("http://" + ip + ":" + port + "/uavWeb/state/reachAreaSensor", setReachAreaSensor); } , 950);
		setInterval( function() { $.getJSON("http://" + ip + ":" + port + "/uavWeb/state/reachAreaAPI", setReachAreaAPI); } , 1000);
		setInterval( function() { $.getJSON("http://" + ip + ":" + port + "/uavWeb/state/reachAreaUser", setReachAreaUser); } , 1050);
		
}


//alter state of the checkbox
function lockMap() {
	lokingMap = document.getElementById("lock").checked;
}

//get the wind predictions
function setWind() {
	WindIntensity = parseFloat(document.getElementById("getWindIntensity").value) * knotsToMS;
	WindBearing = parseFloat(document.getElementById("getWindBearing").value) + 90;	
	WindBearingRads = WindBearing * (Math.PI / 180);
    WindWestUser = WindIntensity * Math.cos(WindBearingRads);
    WindSouthUser = WindIntensity * Math.sin(WindBearingRads);    
}

function changeFuelInput(){
	fuelRate = parseFloat(document.getElementById("fuelType").value);
	fuelTypeUser = true;
}

function resetFuelInput(){
	fuelTypeUser = false;
}



function setReachAreaSensor(data)
{
	if (!data) {
		alert("dados vazios");
        return;
    }	
	var polygonFeature = computePolygon(data.missionFuel, data.fuelLevel, data.gs, data.yaw,
			data.polygonPoints, data.lat, data.lng, data.windWest, data.windSouth, data.FlightTime, "#FF9966");
	reachLayerSensor.removeAllFeatures();
	reachLayerSensor.addFeatures([polygonFeature]);
}
function setReachAreaAPI(data)
{
	if (!data) {
		alert("dados vazios");
        return;
    }	
	var polygonFeature = computePolygon(data.missionFuel, data.fuelLevel, data.gs, data.yaw,
			data.polygonPoints, data.lat, data.lng, data.windWest, data.windSouth, data.FlightTime, "#33CC33");
	reachLayerAPI.removeAllFeatures();
	reachLayerAPI.addFeatures([polygonFeature]);
}
//calculado para cada user com o seu vento proprio
function setReachAreaUser(data)
{
	if (!data) {
		alert("dados vazios");
        return;
    }	
	var polygonFeature = computePolygon(data.missionFuel, data.fuelLevel, data.gs, data.yaw,
			data.polygonPoints, data.lat, data.lng, WindWestUser, WindSouthUser, data.FlightTime, "#FF3399");	
	reachLayerUser.removeAllFeatures();
	reachLayerUser.addFeatures([polygonFeature]);
}	


	


function computePolygon(totalFuel, fuel, velocity, angle, polygonPoints, lat, lng, windWest, windSouth, flightTime, polyColor)
{	
	//alert("totalFuel " + totalFuel + " fuel " + fuel + " velocity " + velocity + " angle " + angle + " polygonPoints " + polygonPoints + " lat " + lat + " lng " + lng + " windWest " + windWest + " windSouth " + windSouth + " flightTime " + flightTime);
	var FuelConsumeRate = 1; // dummy initialization

	//TODO - dummy values - em simulação não há combustivel
	//velocity=60;
	fuel = 2;
	
	if(fuelTypeUser){
		FuelConsumeRate = fuelRate;
	}else{
		var durationInHours = flightTime / 3600; // horas	
		var fuelConsumido = totalFuel - fuel;  // litros
		FuelConsumeRate = fuelConsumido / durationInHours; // litros / horas	
	}
	 
	//Por causa das oscilações iniciais quando se carrega os dados (tempo muito pequeno para as oscilaçoes do fuel)
	if(FuelConsumeRate < 0.01 || FuelConsumeRate > 10) FuelConsumeRate = 10;
	var timeLeft = (fuel/FuelConsumeRate) * 3600; // horas -> seconds

    // meter aqui o formato do timeleft para horas e assim!
	var totalSec = timeLeft;
	var hours = parseInt( totalSec / 3600 ) % 24;
	var minutes = parseInt( totalSec / 60 ) % 60;
	var seconds = parseInt(totalSec % 60);
	var result = (hours < 10 ? "0" + hours : hours) + " H - " + (minutes < 10 ? "0" + minutes : minutes) + " M - " + (seconds  < 10 ? "0" + seconds : seconds) + " S";
	document.getElementById("TimeLeft").innerHTML =  result;

	// Boundary Points to return
	var lats = []; var longs = []; var points = [];
	// auxiliary variables
	var vxCase = 0;	var vyCase = 0; var distCaseX = 0; var distCaseY = 0; var angleRads = 0;

	//compute the points
	for(var i=0; i<polygonPoints; i++)
	{
		angleRads = angle * (Math.PI / 180);
		vxCase = velocity * Math.cos(angleRads) - windWest;
		vyCase = velocity * Math.sin(angleRads) - windSouth; 
				
		distCaseX = (vxCase * timeLeft) / 1000; // km
		distCaseY = (vyCase * timeLeft) / 1000; // km 
						
		// conversao de km para lat e long -> aproximação vista na net
		lats[i] = lat + distCaseY / 110.574;
		longs[i] = lng + distCaseX / (111.320 * Math.cos(lat * (Math.PI / 180)));
		angle = angle + (360 / polygonPoints);
	}

	//convert the lats and longs to usable points
	for (i = 0; i < polygonPoints; i++) {
		var p = new OpenLayers.Geometry.Point(longs[i], lats[i]);
		p.transform(epsg4326, projectTo);
		points.push(p);
	}

	//Define style, points to use -> create Polygon.
	var style_green = {strokeColor: "#ff3300",strokeOpacity: 1,strokeWidth: 2,fillColor: polyColor,fillOpacity: 0.2};	
	var poly = new OpenLayers.Geometry.LinearRing(points);
	var polygonFeature = new OpenLayers.Feature.Vector(poly, null, style_green);
	return polygonFeature;
}



//set airplane map position and heading
function setAirplane(data) 
{
	if (!data) {
        alert("dados vazios");
        return;
    }
	//Extrat position and yaw (roll and pitch are also available) 
	var lat = data.lat;
	var lng = data.lng;
	var position = new OpenLayers.Geometry.Point( lng, lat ).transform(epsg4326, projectTo);
	var yaw = data.yaw;
	
	// Define image as a "feature" of the vector layer:
	var UAVimage = new OpenLayers.Feature.Vector(
			position,
	        {description:'UAV position and heading'} ,
	        {externalGraphic: 'http://www.clipartbest.com/cliparts/4cb/o7k/4cbo7kkzi.png', graphicHeight: 50, graphicWidth: 30, graphicXOffset:-15, graphicYOffset:-25 , rotation: yaw}
	);  	

	//add image to the layer
	imageLayer.removeAllFeatures();
	imageLayer.addFeatures(UAVimage);
	
	if(lokingMap){
		//Update the map center - centered in the airplane
		var lonLat = new OpenLayers.LonLat(lng, lat).transform(epsg4326, projectTo);
	    map.setCenter(lonLat);
	}
}





//center the map on the airplane position
function centerMapOnPlane(data) 
{
	if (!data) {
        alert("dados vazios");
        return;
    }
	var lat = data.lat;
	var lng = data.lng;
	var lonLat = new OpenLayers.LonLat(lng, lat).transform(epsg4326, projectTo);
    map.setCenter(lonLat);
}




//get time for the start of the flight
/*
function startFlight(){
	 startFlighttimeInMs =  Date.now();
}
*/

/*
// serve para desenhar o poligono quando recebo todos os pontos diretamente
// add the UAV reachable area 
function plotPolygon(data) 
{		
	//Get JSON data
		if (!data) {
	        alert("dados vazios");
	        return;
	    }
		
		var lats = [];
		var longs = [];
		var points = [];
	
		//Take the latitude and longitude of the 8 bounding points
		for (var i = 0; i < data.length; i++) {
		    lats.push(data[i].lat);
		    longs.push(data[i].lon);
		}
		
		//convert the lats and longs to usable points
		for (i = 0; i < lats.length; i++) {
			p = new OpenLayers.Geometry.Point(longs[i], lats[i]);
			p.transform(epsg4326, projectTo);
			points.push(p);
		}

		//Define style, points to use -> create Polygon.
		var style_green = {strokeColor: "#ff3300",strokeOpacity: 1,strokeWidth: 2,fillColor: "#FF9966",fillOpacity: 0.2};	
		var poly = new OpenLayers.Geometry.LinearRing(points);
		var polygonFeature = new OpenLayers.Feature.Vector(poly, null, style_green);
		
		return polygonFeature;
}
 */




//Add a selector control to the vectorLayer with popup functions
/*
 
  	//Add a selector control to the vectorLayer with popup functionn - in main
	    //setLayerControl();  
	   	    
	    
function setLayerControl() {

		var controls = {
	      selector: new OpenLayers.Control.SelectFeature(vectorLayer, { onSelect: createPopup, onUnselect: destroyPopup })
	    };

	    function createPopup(feature) {
	      feature.popup = new OpenLayers.Popup.FramedCloud("pop",
	          feature.geometry.getBounds().getCenterLonLat(),
	          null,
	          '<div class="markerContent">'+feature.attributes.description+'</div>',
	          null,
	          true,
	          function() { controls['selector'].unselectAll(); }
	      );
	      //feature.popup.closeOnMove = true;
	      map.addPopup(feature.popup);
	    }

	    function destroyPopup(feature) {
	      feature.popup.destroy();
	      feature.popup = null;
	    }
	    
	    map.addControl(controls['selector']);
	    controls['selector'].activate();    
}	    
*/



/* TODO para poder usar cookies
function setCookie()
{
    document.cookie = "lock=" + false; 
    document.cookie = "startFlight=" + false;
    document.cookie = "WindWest=" + 0;
    document.cookie = "WindSouth=" + 0;
}

function getCookie(cname){
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}
*/
