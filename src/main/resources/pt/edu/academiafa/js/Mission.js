/**
 * Mission data - right side of the site containing information about the flight and wind conditions
 */

//var ip = "172.21.2.162";
//var ip = "192.168.106.95";
var ip = "localhost";
var distanceTotal = 0;

var port = "8080";

function startMission()
{
		//alert("entrou no MapStack");
		$.getJSON("http://" + ip + ":" + port + "/uavWeb/state/missionEstimate", printMissionData);
		$.getJSON("http://" + ip + ":" + port + "/uavWeb/state/weather", printWeather);

		setInterval( function() { $.getJSON("http://" + ip + ":" + port + "/uavWeb/state/missionEstimate", printMissionData); } , 1000);
		setInterval( function() { $.getJSON("http://" + ip + ":" + port + "/uavWeb/state/weather", printWeather); } , 600000); // 10 minutes
}
	

	
	function printWeather(data)
	{
		if (!data) {
	        alert("dados vazios");
	        return;
	    }
		var wests = [];
		var souths = [];
		
		//Take the latitude and longitude of the 8 bounding points
		for (var i = 0; i < data.length; i++) {
		    wests.push(data[i].windWest);
		    souths.push(data[i].windSouth);
		}
				
		var table = document.getElementById("winds");
		
		// clear table 		
		if(table.rows.length > 1)
			for(i=0; i < data.length; i++){
				//alert("AAA: i: " + i);
				table.deleteRow(1);   		
			}
				
		if(table.rows.length < 2){
			// fill table
			for(i=0; i < data.length; i++)
			{
				var row = table.insertRow(i+1);
			    var cell1 = row.insertCell(0);
			    var cell2 = row.insertCell(1);
			    var cell3 = row.insertCell(2);
			    
			    cell1.innerHTML = i+1;
			    cell2.innerHTML = souths[i];
			    cell3.innerHTML = wests[i];		
			}
		}
		
	}
	
	
	
	
	
	function printMissionData(data)
	{
		if (!data) {
	        alert("dados vazios");
	        return;
	    }
		document.getElementById("TAS").innerHTML = parseFloat(data.TAS).toFixed(2);
		document.getElementById("GS").innerHTML = parseFloat(data.GS).toFixed(2);
		
		document.getElementById("DistanceCovered").innerHTML = parseFloat(data.DistanceCovered).toFixed(3);
		
		document.getElementById("ETAPortoSanto").innerHTML = data.ETAPortoSanto;
		document.getElementById("ETASelvagens").innerHTML = data.ETASelvagens;
	
		// TimeLeft passou a ser calculado na parte da missão		
		// document.getElementById("Fuel").innerHTML = parseFloat(data.Fuel).toFixed(3);
		document.getElementById("EstimatedFuel").innerHTML = parseFloat(data.EstimatedFuel).toFixed(3);	
		document.getElementById("WindWest").innerHTML = parseFloat(data.WindWest).toFixed(2);
		document.getElementById("WindSouth").innerHTML = parseFloat(data.WindSouth).toFixed(2);		
		
		var timeSeconds = parseInt(data.FlightTime);
		var hours = parseInt( timeSeconds / 3600 ) % 24;
		var minutes = parseInt( timeSeconds / 60 ) % 60;
		var seconds = parseInt(timeSeconds % 60);
		var result = (hours < 10 ? "0" + hours : hours) + " H - " + (minutes < 10 ? "0" + minutes : minutes) + " M - " + (seconds  < 10 ? "0" + seconds : seconds) + " S";
		document.getElementById("FlightTime").innerHTML = result;
	
	}	
	
	
	/*
	var GS = parseFloat(data.GS);
	var interval = parseFloat(data.Interval).toFixed(5);	
	var distance = GS / 3600;
	distanceTotal = distanceTotal + distance;
	document.getElementById("DistanceCovered").innerHTML = distanceTotal.toFixed(3);
	*/
	