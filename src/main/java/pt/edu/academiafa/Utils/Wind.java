package pt.edu.academiafa.Utils;

/*
import com.arcusweather.forecastio.ForecastIO;
import com.arcusweather.forecastio.ForecastIODataPoint;
import com.arcusweather.forecastio.ForecastIOResponse;
import java.util.List;
*/

import com.github.dvdme.ForecastIOLib.FIOCurrently;
import com.github.dvdme.ForecastIOLib.FIOHourly;
import com.github.dvdme.ForecastIOLib.ForecastIO;


public class Wind {
	
	private static int numberOfHours = Params.getNumberOfWindHours();	
	private static String APIkey = Params.getAPIKey();
	private static final float msToKnots = 1.94384f; // m/s to knots conversion factor

	private static float windSouth;
	private static float windWest;

	/**
	 * @param lat Latitude of which the wind relates to.
	 * @param lng Longitude of which the wind relates to.
	 * @return Vector of wind intensity (in km/h) for each hour, in South and West directions. 
	 */
	public static double[] getWinds(float lat, float lng)
	{
		// the service allows us to receive hourly data from 48 hours
		numberOfHours = Params.getNumberOfWindHours();
		if(numberOfHours > 47) numberOfHours = 47;
		
		float[] windSpeed = new float[numberOfHours];
		float[] windBearing = new float[numberOfHours];
		double[] winds = new double[2*numberOfHours];
		
		String latitude = String.valueOf(lat);
		String longitude = String.valueOf(lng);
				
    	ForecastIO fio = new ForecastIO(APIkey);
        fio.setUnits(ForecastIO.UNITS_SI);
        fio.setLang(ForecastIO.LANG_ENGLISH);
        fio.getForecast(latitude, longitude);   
        
        // to get current situation
        FIOCurrently currently = new FIOCurrently(fio);
    
        float currWind = Float.parseFloat(currently.get().getByKey("windSpeed"));
        float currBearing = Float.parseFloat(currently.get().getByKey("windBearing"));

        windSouth = (float) (currWind * Math.sin(Math.toRadians(currBearing))); 
        windWest = (float) (currWind * Math.cos(Math.toRadians(currBearing)));
		
        // To get some hourly predictions
        FIOHourly hourly = new FIOHourly(fio);
        
        //In case there is no hourly data available
        if(hourly.hours()<0) Params.log.error("No hourly data obtained.");
        else Params.log.warn("Hours to check wind " + hourly.hours());
        
        //Get hourly data
        for(int i = 0; i< numberOfHours; i++)
        {
            //String [] h = hourly.getHour(i).getFieldsArray();
        	windSpeed[i] = Float.parseFloat(hourly.getHour(i).getByKey("windSpeed"));
        	windBearing[i] = Float.parseFloat(hourly.getHour(i).getByKey("windBearing"));	

            winds[2*i] = windSpeed[i] * Math.sin(Math.toRadians(windBearing[i])) * msToKnots; 
			winds[2*i+1] = windSpeed[i] * Math.cos(Math.toRadians(windBearing[i])) * msToKnots;
        }
   		return winds;
	}


	public static float getWindSouth() {
		return windSouth;
	}

	public static float getWindWest() {
		return windWest;
	}	

}
