package pt.edu.academiafa.Utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;


public final class Params {
	
	public final static Logger log = Logger.getLogger("SharpeyeVisualizer");
	
    // global system variables initialized with default values
    private static int fuelFilterReadings = 20;
	private static int polygonPoints = 8; 
	private static boolean centerMapOnAirplane = true;
	private static int centeringPeriod = 20;
	private static int numberOfWindHours = 8;
	private static String APIKey = "4284fef30e7c7831562f368feb71cc64";
	private static String ROSip = "localhost";
	private static int ROSport = 11311;

	public Params(){} // block construction
	
	// get global system parameters 
    static
    {
    	Properties prop = new Properties();
		InputStream input = null;
		
		try {
			input = new FileInputStream("parameters.properties");		
			prop.load(input); // load a properties file
			fuelFilterReadings = Integer.parseInt(prop.getProperty("fuelFilterReadings"));
			polygonPoints = Integer.parseInt(prop.getProperty("polygonPoints"));
			numberOfWindHours =  Integer.parseInt(prop.getProperty("numberOfWindHours"));
			APIKey = prop.getProperty("APIKey");
			ROSip = prop.getProperty("ROSip");
			ROSport =  Integer.parseInt(prop.getProperty("ROSport"));
											
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			log.warn("loaded parameters configuration successfully");
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
    }
    
 // getters - no need to allow setters
 	public static int getFuelFilterReadings() {
 		return fuelFilterReadings;
 	}

 	public static int getPolygonPoints() {
 		return polygonPoints;
 	}

 	public static boolean isCenterMapOnAirplane() {
 		return centerMapOnAirplane;
 	}

 	public static int getCenteringPeriod() {
 		return centeringPeriod;
 	}

 	public static int getNumberOfWindHours() {
 		return numberOfWindHours;
 	}
 	
 	public static String getAPIKey() {
 		return APIKey;
 	}

	public static String getROSip() {
		return ROSip;
	}

	public static int getROSport() {
		return ROSport;
	}
	
}
