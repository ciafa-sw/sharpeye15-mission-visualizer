package pt.edu.academiafa.Utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

//coordenadas da ilhas Porto santo - deverao ser lidas em ficheiro de inicialização
	//public static final double PortoSantoLat = -16.35;
	//public static final double PortoSantoLng = 33.074;
//ilha Selvagem grande
	//public static final double SelvagensLat = -15.863;
	//public static final double SelvagensLng = 30.145;

public class MissionEstimate {
		
	//Constants 
	private static final int R = 6371; // Radius of the earth in km
	private static final float msToKnots = 1.94384f; // m/s to knots conversion factor
	
	// defs
	private static final SimpleDateFormat ft = new SimpleDateFormat (" HH:mm 'of' yyyy.MM.dd ");
	private static final long ONE_MINUTE_IN_MILLIS = 60000;//millisecs		
		
	// auxiliary values to compute the displayed values
	private double currLat;
	private double currLng;

	// values to display to the user
	private float TAS;
	private float GS;
	
	private float DistanceCovered;
	
	private String ETAOrigin;
	private String ETADestination;
	
	private String TimeLeft;
	private float IntervalTime;

	private float Fuel;
	private float EstimatedFuel;

	private float WindWest;
	private float WindSouth;
	
	private int FlightTime;
	
	//move these to the mission.paramenters
	private static double missionFuel = 5; // default
	private static double fuelDensity = 0.75; // default
	
	
	// using the ‘Haversine’ formula to compute distance from two (lat,lon) points
	public static double getDistanceFromLatLonInKm(double lat1,double lon1,double lat2,double lon2) 
	{	
		  double dLat = Math.toRadians(lat2-lat1);  // deg2rad below
		  double dLon = Math.toRadians(lon2-lon1); 
		  double a = 
		    Math.sin(dLat/2) * Math.sin(dLat/2) +
		    Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * 
		    Math.sin(dLon/2) * Math.sin(dLon/2)
		    ; 
		  double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
		  double d = R * c; // Distance in km
		  return d;
	}
	


	public MissionEstimate(TelemetryFuelNode t)
	{
		// inicializations 
		currLat = t.getLatitude();
		currLng = t.getLongitude();
		WindWest = t.getWindWest();
		WindSouth = t.getWindSouth();
		// valores calculados - nao lidos
		TAS = t.getTAS();
		GS = t.getGS();
		DistanceCovered = t.getDistanceCovered();
		IntervalTime = t.getIntervalTime();
		
		//Fuel values
		Fuel = t.getFuelInstant(); // instant readings
		EstimatedFuel = t.getFuelLevel(); // está estimado com um filtro!!!
		FlightTime = t.getElapsedTime();

		// converter para Km/h para o display em km (o tempo é calculado com base na vel em km/h)
		TAS = (float) (TAS*msToKnots);
		GS = (float) (GS*msToKnots);
		WindWest = (float) (WindWest * msToKnots);
		WindSouth = (float) (WindSouth * msToKnots);
		
		// get the LatLong of the origin and destination
		double originLat = 0, originLong = 0, destinationLat = 0, destinationLong = 0;
		Properties prop = new Properties();
		InputStream input = null;

		try {
	        Params.log.warn("Atempting to load mission data");
			input = new FileInputStream("mission.properties");		
			prop.load(input); // load a properties file
			originLat = Double.parseDouble(prop.getProperty("originLat"));
			originLong = Double.parseDouble(prop.getProperty("originLong"));
			destinationLat = Double.parseDouble(prop.getProperty("destinationLat"));
			destinationLong = Double.parseDouble(prop.getProperty("destinationLong"));
			missionFuel = Double.parseDouble(prop.getProperty("fuelQuantity"));
			fuelDensity = Double.parseDouble(prop.getProperty("fuelDensity"));

		} catch (IOException ex) {
	        Params.log.error("Failed to load mission data");
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
	        Params.log.warn("Successfully loaded mission data");
		}

		// tempos estimados à origem - usando velocidade atual - GS
		double dist = getDistanceFromLatLonInKm(originLat, originLong, currLat, currLng);		
		double hoursLeft = dist / GS;
		int minutesLeft = (int) Math.round(hoursLeft * 60);		    		
		Date now = new Date();
		long now_in_millis = now .getTime();
		Date datePS = new Date(now_in_millis + (minutesLeft * ONE_MINUTE_IN_MILLIS));
		ETAOrigin = ft.format(datePS);
		
		// tempos estimados ao destino - usando velocidade atual - GS
		dist = getDistanceFromLatLonInKm(destinationLat, destinationLong, currLat, currLng);			
		hoursLeft = dist / GS;
		minutesLeft = (int) Math.round(hoursLeft * 60);		    		
		Date dateSelv = new Date(now_in_millis + (minutesLeft * ONE_MINUTE_IN_MILLIS));
		ETADestination = ft.format(dateSelv);
		
	}

	
		//getters
		public float getTAS() {
			return TAS;
		}
		
		public float getGS() {
			return GS;
		}
		
		public String getETAOrigin() {
			return ETAOrigin;
		}
		
		public String getETADestination() {
			return ETADestination;
		}
		
		public float getFuel() {
			return Fuel;
		}

		public float getEstimatedFuel() {
			return EstimatedFuel;
		}

		public float getWindWest() {
			return WindWest;
		}

		public float getWindSouth() {
			return WindSouth;
		}
	
		public static double getMissionFuel() {
			return missionFuel;
		}

		public String getTimeLeft() {
			return TimeLeft;
		}
		
		public float getIntervalTime() {
			return IntervalTime;
		}

		public float getDistanceCovered() {
			return DistanceCovered;
		}

		public static double getFuelDensity() {
			return fuelDensity;
		}

		public int getFlightTime() {
			return FlightTime;
		}

}
