package pt.edu.academiafa.Utils; 

public class Airplane{
	
	double lat;
	double lng;
	int roll;
	int pitch;
	float yaw;
	
	public Airplane(TelemetryFuelNode t) {
		this.lat = t.getLatitude();
		this.lng = t.getLongitude();
		this.roll = t.getRoll();
		this.pitch = t.getPitch();
		this.yaw = t.getYaw();
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public int getRoll() {
		return roll;
	}

	public void setRoll(int roll) {
		this.roll = roll;
	}

	public int getPitch() {
		return pitch;
	}

	public void setPitch(int pitch) {
		this.pitch = pitch;
	}

	public float getYaw() {
		return yaw;
	}

	public void setYaw(float yaw) {
		this.yaw = yaw;
	}
	
}