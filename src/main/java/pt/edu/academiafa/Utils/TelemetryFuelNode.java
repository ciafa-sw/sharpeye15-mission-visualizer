
package pt.edu.academiafa.Utils;

import java.util.Arrays;
import org.apache.log4j.Logger;
import org.ros.message.MessageListener;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.NodeMain;
import org.ros.node.topic.Subscriber;

import pt.edu.academiafa.Utils.Params;
import seagull_autopilot_msgs.AutopilotADCSamples;
import seagull_autopilot_msgs.AutopilotTelemetry;
import seagull_autopilot_msgs.AutopilotStatus;

/**
 * A simple {@link Subscriber} {@link NodeMain}.
 * @author Daniel Soares
 */

public class TelemetryFuelNode extends AbstractNodeMain {
	
	 static Logger logger = Logger.getLogger(TelemetryFuelNode.class);
	 	 
	 private float latitude;	
	 private float longitude;
	 private float altitude;
	 private int ias;
	 private int vx;
	 private int vy;
	 private int vz;
	 private int roll;
	 private int pitch;
	 private float yaw;
	 private int barometricAltitude;
	 private float windSouth;
	 private float windWest;
	 private int leftRPM;
	 private int rightRPM;
	 private int staticPressure;
	 private int accelX;
	 private int accelY;
	 private int accelZ;
	 private int compass;
	 private int agl;
	 private long timestamp; 	
	 private float fuelLevel;
	 private float fuelInstant;

	//campos colocados aqui para nao precisar de estar a calculá-lo em diferentes funcoes
	 private float TAS, GS;
	 
	 //Tempo em segundos entre leituras consecutivas de pacote telemetria
	 private float IntervalTime = 0;
 
	 //time since the take off
	 private int elapsedTime;
	 // Elapsed Time in reading -1 -> to get the transition meaning a new flight started
	 private int pastElapsedTime = 0; 

	// auxiliar variables
	 private int indexOfReading = 0;
	 private int totalReadings = Params.getFuelFilterReadings(); // counter of times it was read a value
	 private float[] FuelReadings = new float[totalReadings]; // vector with the fuel readings
	 
	 private long start = System.nanoTime();
	 private long time = 0;
	 private float DistanceCovered = 0;

	 public GraphName getDefaultNodeName() {
	    return GraphName.of("Rosjava_Telemetry_Fuel_listener");
	 }

	    
	   
	   
	  @Override
	  public void onStart(ConnectedNode connectedNode) 
	  {  
		 logger.warn("Node " + this.getDefaultNodeName() + " initialized");

	     // subscriber para a telemetria 
	     Subscriber<AutopilotTelemetry> subscriberTelemetry = connectedNode.newSubscriber("autopilot_telemetry", AutopilotTelemetry._TYPE);
	  
	     subscriberTelemetry.addMessageListener(new MessageListener<AutopilotTelemetry>() {
	       public void onNewMessage(AutopilotTelemetry msg) {
	  
    	  	 setLatitude(msg.getLatitude());
    		 setLongitude(msg.getLongitude());
    		 setAltitude(msg.getAltitude());
    		 setIas(msg.getIas());
    		 setVx(msg.getVx());
    		 setVy(msg.getVy());
    		 setVz(msg.getVz());
    		 setRoll(msg.getRoll());
    		 setPitch(msg.getPitch());
    		 setYaw(msg.getYaw());
    		 setBarometricAltitude(msg.getBarometricAltitude());
    		 setWindSouth(msg.getWindSouth());
    		 setWindWest(msg.getWindWest());
    		 setLeftRPM(msg.getLeftRPM());
    		 setRightRPM(msg.getRightRPM());
    		 setStaticPressure(msg.getStaticPressure());
    		 setAccelX(msg.getAccelX());
    		 setAccelY(msg.getAccelY());
    		 setAccelZ(msg.getAccelZ());
    		 setCompass(msg.getCompass());
    		 setAgl(msg.getAgl());
    		 setTimestamp(msg.getTimestamp());
    		 
    		 //setFuelLevel(msg.getFuel());
    		 updateGS();
    		 
    		 // calculate the time between readings - in order to integrate distance
   	         time = (System.nanoTime() - start);
   	         start = System.nanoTime();
    	     //Params.log.warn("\n Took " + (time / 1e9) + " seconds to receive a new telemetry message  ");
    	      
   	         IntervalTime = (float) (time / 1e9); 
   	         updateDistance();
   	         //Params.log.warn("Heard AutopilotTelemetry: example Latitude: " + msg.getLatitude());
	      }
   
	    });
	    
	     
		// subscriber para o combustivel 
	    Subscriber<AutopilotADCSamples> subscriberFuel = connectedNode.newSubscriber("autopilot_adc_samples", AutopilotADCSamples._TYPE);
	    
	    subscriberFuel.addMessageListener(new MessageListener<AutopilotADCSamples>() {
	      public void onNewMessage(AutopilotADCSamples msg2) 
	      {
	    	 //estimação
	    	 addFuelReading(msg2.getAnalog1() / (float) MissionEstimate.getFuelDensity());
	    	 //leitura direta
	    	 setFuelInstant(msg2.getAnalog1() / (float) MissionEstimate.getFuelDensity());
    	     //Params.log.warn("Heard AutopilotADCSamples: example: getAnalog1: " + msg2.getAnalog1());
	      }
	    });
	    
	   
		// subscriber para o Flight time - elapsed time 
	    Subscriber<AutopilotStatus> subscriberStatus = connectedNode.newSubscriber("autopilot_status", AutopilotStatus._TYPE);
	    
	    subscriberStatus.addMessageListener(new MessageListener<AutopilotStatus>() {
	      public void onNewMessage(AutopilotStatus msg3) 
	      {
	    	 //elapsed Time vem em dezenas de segundos
	    	 setElapsedTime((int) msg3.getElapsedTime() * 10);
    	     Params.log.warn("Heard AutopilotStatus: elapsedTime: " + msg3.getElapsedTime());
    	     //System.out.println("Heard AutopilotStatus: elapsedTime: " + msg3.getElapsedTime());
	      }
	    }); 
	    
	  }

	
	    
		// computes the value of GS so it does not have to be compute in each part it is needed
		  private void updateGS()
		  {
			  // estas formulas sao simplificadas - ver se é preciso algo mais complexo
				TAS = (float) (ias * (1 + ((altitude/1000) * .02)));
				GS = (float) Math.sqrt(vx*vx + vy*vy);
		  }
		  
		  
		  
		// computes the total distance covered
		  private void updateDistance()
		  {
			  // despoletar a limpeza da distancia percorrida assim que inicia novo voo
			  if(pastElapsedTime == 0 && elapsedTime != 0) DistanceCovered = 0;
			  // Convertendo tbm para milhas
			  DistanceCovered = DistanceCovered + ((GS * IntervalTime) / 1852);
			  // updating state
			  pastElapsedTime = elapsedTime;
			  //System.out.println("DistanceCovered: " + DistanceCovered + " GS: " + GS + " IntervalTime: " + IntervalTime);
		  }
		  
		  
	  
	  // gets a fuel reading and updates the Fuel as the median value
	  private void addFuelReading(float d)
	  {
		  FuelReadings[indexOfReading % totalReadings] = d;
		  indexOfReading ++;

		  float[] orderedFuel;
		  if(indexOfReading < totalReadings) orderedFuel= new float[indexOfReading];	
		  else orderedFuel= new float[totalReadings];
		  
		  System.arraycopy( FuelReadings, 0, orderedFuel, 0, orderedFuel.length );
		  Arrays.sort( orderedFuel );
		  
		  float middle = orderedFuel.length/2;
		  int half = (int) Math.floor(middle);
		   
		  if( middle == (int) middle )
			  setFuelLevel(orderedFuel[half]);
		  else
			  setFuelLevel((float) ((orderedFuel[half-1] + orderedFuel[half]) / 2.0));
		  }
	  
	  
	  
	  // getters and setters for the telemetry message 
		public static Logger getLogger() {
			return logger;
		}

		public static void setLogger(Logger logger) {
			TelemetryFuelNode.logger = logger;
		}

		public float getLatitude() {
			return latitude;
		}

		public void setLatitude(float latitude) {
			this.latitude = latitude;
		}

		public float getLongitude() {
			return longitude;
		}

		public void setLongitude(float longitude) {
			this.longitude = longitude;
		}

		public float getAltitude() {
			return altitude;
		}

		public void setAltitude(float altitude) {
			this.altitude = altitude;
		}

		public int getIas() {
			return ias;
		}

		public void setIas(int ias) {
			this.ias = ias;
		}

		public int getVx() {
			return vx;
		}

		public void setVx(int vx) {
			this.vx = vx;
		}

		public int getVy() {
			return vy;
		}

		public void setVy(int vy) {
			this.vy = vy;
		}

		public int getVz() {
			return vz;
		}

		public void setVz(int vz) {
			this.vz = vz;
		}

		public int getRoll() {
			return roll;
		}

		public void setRoll(int roll) {
			this.roll = roll;
		}

		public int getPitch() {
			return pitch;
		}

		public void setPitch(int pitch) {
			this.pitch = pitch;
		}

		public float getYaw() {
			return yaw;
		}

		public void setYaw(float yaw) {
			this.yaw = yaw;
		}

		public int getBarometricAltitude() {
			return barometricAltitude;
		}

		public void setBarometricAltitude(int barometricAltitude) {
			this.barometricAltitude = barometricAltitude;
		}

		public float getWindSouth() {
			return windSouth;
		}

		public void setWindSouth(float windSouth) {
			this.windSouth = windSouth;
		}

		public float getWindWest() {
			return windWest;
		}

		public void setWindWest(float windWest) {
			this.windWest = windWest;
		}

		public int getLeftRPM() {
			return leftRPM;
		}

		public void setLeftRPM(int leftRPM) {
			this.leftRPM = leftRPM;
		}

		public int getRightRPM() {
			return rightRPM;
		}

		public void setRightRPM(int rightRPM) {
			this.rightRPM = rightRPM;
		}

		public int getStaticPressure() {
			return staticPressure;
		}

		public void setStaticPressure(int staticPressure) {
			this.staticPressure = staticPressure;
		}

		public int getAccelX() {
			return accelX;
		}

		public void setAccelX(int accelX) {
			this.accelX = accelX;
		}

		public int getAccelY() {
			return accelY;
		}

		public void setAccelY(int accelY) {
			this.accelY = accelY;
		}

		public int getAccelZ() {
			return accelZ;
		}

		public void setAccelZ(int accelZ) {
			this.accelZ = accelZ;
		}

		public int getCompass() {
			return compass;
		}

		public void setCompass(int compass) {
			this.compass = compass;
		}

		public int getAgl() {
			return agl;
		}

		public void setAgl(int agl) {
			this.agl = agl;
		}

		public long getTimestamp() {
			return timestamp;
		}

		public void setTimestamp(long timestamp) {
			this.timestamp = timestamp;
		}

		// Fuel getters and setters
		public float getFuelLevel() {
			return fuelLevel;
		}

		public void setFuelLevel(float fuelLevel) {
			this.fuelLevel = fuelLevel;
		}
		
		// valores calculados - nao lidos
	    public float getTAS() {
	 		return TAS;
		}

		public void setTAS(float tAS) {
			TAS = tAS;
		}

		public float getGS() {
			return GS;
		}

		public void setGS(float gS) {
			GS = gS;
		}
	    
		public float getFuelInstant() {
			return fuelInstant;
		}

		public void setFuelInstant(float d) {
			this.fuelInstant = d;
		}
		
		public float getIntervalTime() {
			return IntervalTime;
		}

		public float getDistanceCovered() {
			return DistanceCovered;
		}
		
		public int getElapsedTime() {
			return elapsedTime;
		}

		public void setElapsedTime(int elapsedTime) {
			this.elapsedTime = elapsedTime;
		}
		
	}



/*
// just to check the time it takes to get a reading
time = (System.nanoTime() - start);
start = System.nanoTime();
 Params.log.warn("\n Took %.3f seconds to receive a new ADC_samples message  " + (time / 1e9));
 */



/* this is wrong since vx is already a component of GS on xx - this is for vx as a IAS component

float TASx = (float) (vx * (1 + (altitude/1000 * .02)));
float TASy = (float) (vy * (1 + (altitude/1000 * .02)));

float GSx = TASx + windWest;
float GSy = TASy + windSouth;
*/


/*
try { updateDistanceCovered();
} catch (FileNotFoundException e) {	e.printStackTrace();
} catch (UnsupportedEncodingException e) { e.printStackTrace();
}
*/

/*
// reads the distance covered in the file and updates it 
private void updateDistanceCovered() throws FileNotFoundException, UnsupportedEncodingException
{
  //PrintWriter writer = new PrintWriter("uavWebMemory.txt", "UTF-8");
  //writer.println("128.235454");
  //writer.println("Took %.3f seconds to receive a new ADC_samples message  " + (time / 1e9));
  //writer.close();
  
  counter ++;
  
    // The name of the file to open.
    String fileName = "uavWebMemory.txt";
    // This will reference one line
    String line = null;

    try {
        // FileReader reads text files in the default encoding.
        FileReader fileReader = new FileReader(fileName);
        // Always wrap FileReader in BufferedReader.
        BufferedReader bufferedReader = new BufferedReader(fileReader); 
        line = bufferedReader.readLine();
        bufferedReader.close();         
    }
    catch(FileNotFoundException ex) {
        System.out.println("Unable to open file '" + fileName + "'");                
    }
    catch(IOException ex) {
        System.out.println("Error reading file '" + fileName + "'");                  
    }
    
    System.out.println("counter is: " + counter + " GS was: " + GS + " time was " + (time / 1e9) + " and initial distance : " + DistanceCovered );

    DistanceCovered = Float.parseFloat(line);
    float increment = (float) (GS * (time / 1e9));
    DistanceCovered += increment;
    String stringDistance = Float.toString(DistanceCovered);
    
    System.out.println(" increment was : " + increment + " and distance became: " + DistanceCovered );
    
    try {
        // Assume default encoding.
        FileWriter fileWriter = new FileWriter(fileName);
        // Always wrap FileWriter in BufferedWriter.
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        // Note that write() does not automatically append a newline character.
        bufferedWriter.write(stringDistance);
        bufferedWriter.newLine();
        bufferedWriter.close();
    }
    catch(IOException ex) {
        System.out.println("Error writing to file '" + fileName + "'");
    }

}
*/
