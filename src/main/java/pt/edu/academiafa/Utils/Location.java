package pt.edu.academiafa.Utils; 

public class Location{
	
	private double Latitude;
	private double Longitude;
	
	public Location(){
		this.Latitude = 0;
		this.Longitude = 0;
	}
	
	public Location(double lat, double lng){
		this.Latitude = lat;
		this.Longitude = lng;
	}

	public double getLatitude() {
		return Latitude;
	}

	public void setLatitude(double latitude) {
		Latitude = latitude;
	}

	public double getLongitude() {
		return Longitude;
	}

	public void setLongitude(double longitude) {
		Longitude = longitude;
	}

	@Override
	public String toString() {
		return "Location [Latitude=" + Latitude + ", Longitude=" + Longitude + "]";
	}
	
}