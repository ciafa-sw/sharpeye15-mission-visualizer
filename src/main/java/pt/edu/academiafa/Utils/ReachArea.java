
package pt.edu.academiafa.Utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import pt.edu.academiafa.Utils.Params;

public class ReachArea {
	
	static double lat;
	static double lng;
	static float GS; 
	static float windW;
	static float windS;	
	static double fuel; 
	static float angle;    
	
	public ReachArea(){}
	
	public ReachArea(TelemetryFuelNode t) 
	{
		//initializations
		lat = t.getLatitude();
		lng = t.getLongitude();
		
		GS = t.getGS();
		windW = t.getWindWest();
		windS = t.getWindSouth();	
		//Leitura de fuel usada deriva do filtro de mediana usado
		fuel = t.getFuelLevel();
		angle = t.getYaw();     	
	}

	
	public List<Location> getBoundaryPointsSensor(){
		return getBoundaryPoints(windW, windS);
	}

	public List<Location> getBoundaryPointsAPI(){
		return getBoundaryPoints(Wind.getWindWest(), Wind.getWindSouth());
	}
	
	//pôr aqui os valores lidos na pagina web - caso se mude para computar isto aqui 
	// os valores (2, 2) são apenas valores de wind de teste
	public List<Location> getBoundaryPointsUser(){
		return getBoundaryPoints(2, 2);
	}
		
		
	/*
	 * Não está atualizada - como não é usada não a mudei. Ver mapa.js para aceder ao algoritmo correto
	 */
	public List<Location> getBoundaryPoints(float windWest, float windSouth )
	{
		double totalFuel = MissionEstimate.getMissionFuel();
		double fuelConsumido = totalFuel - fuel;
		
		Date startMission = new Date();
		Date now = new Date();
		long duration = now.getTime() - startMission.getTime();
		long durationInHours = duration / (1000*60);			
		double FuelConsumeRate = fuelConsumido / durationInHours; // 0.5 l/h

		double timeLeft = fuel/FuelConsumeRate;	
		
		double velocity = GS;
		// Boundary Points to return
		List<Location> points = new ArrayList<Location>();					
		// auxiliary variables
		double vxCase = 0;
		double vyCase = 0;
		double distCaseX = 0;
		double distCaseY = 0;	
		//get the number of points to use - initialize by properties file
		int polygonPoints = Params.getPolygonPoints();
		
		//compute the points
		for(int i=0; i<polygonPoints; i++)
		{
			vxCase =  velocity*Math.cos(Math.toRadians(angle)) - windWest;
			vyCase =  velocity*Math.sin(Math.toRadians(angle)) - windSouth; 
			
			distCaseX = vxCase * timeLeft;
			distCaseY = vyCase * timeLeft;
			
			// conversao de km para lat e long -> aproximação vista na net
			Location loc = new Location();
			loc.setLatitude(lat + distCaseX/110.574);
			loc.setLongitude(lng + distCaseY/(111.320*Math.cos(Math.toRadians(lat))));
			
			points.add(loc);		
			angle += (360 / polygonPoints);
		}
		return points;
	}
	
	
}
