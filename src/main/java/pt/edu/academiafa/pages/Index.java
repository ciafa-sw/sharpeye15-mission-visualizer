package pt.edu.academiafa.pages;

import java.net.URI;
import java.net.URISyntaxException;
import org.apache.tapestry5.annotations.*;
import org.apache.tapestry5.ioc.annotations.*;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;
import org.ros.address.InetAddressFactory;
import org.ros.node.NodeConfiguration;

import pt.edu.academiafa.Utils.Params;


/**
 * Start page of application uavWeb.
 */
public class Index
{
    @Inject
    private JavaScriptSupport js;

    /**
     * import the Map JavaScript stack.
     */
    @SetupRender
    public void importStack()
    {
        js.importStack("map");
        js.importStack("mission");     
    }
	
	private static URI getMasterUri() {
		String ROSip = Params.getROSip();
		int ROSport = Params.getROSport();
		try {
	        Params.log.warn("Atempting connection to roscore on ip: " + ROSip + " and port: " + ROSport);
			return new URI("http", null, ROSip, ROSport, "/", null, null);
		} catch (URISyntaxException e) {
	        Params.log.error("Atempting connection to roscore on ip: " + ROSip + " and port: " + ROSport);
			return null;
		} finally {
	        Params.log.warn("Connection to roscore successful");
		}
	}
	
	static NodeConfiguration setupConfiguration() {
		NodeConfiguration nodeConfiguration = NodeConfiguration.newPublic( InetAddressFactory.newNonLoopback().getHostName(), getMasterUri());
		return nodeConfiguration;
	}

}
