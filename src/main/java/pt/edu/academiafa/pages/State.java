/*
 * This code is part of the BigActor project.
 *
 * Copyright (c) 2013 Clemens Krainer <clemens.krainer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package pt.edu.academiafa.pages;

import pt.edu.academiafa.services.JsonConverter;

import pt.edu.academiafa.Utils.Airplane;
import pt.edu.academiafa.Utils.MissionEstimate;
import pt.edu.academiafa.Utils.Params;
//import pt.edu.academiafa.Utils.ReachArea;
import pt.edu.academiafa.Utils.TelemetryFuelNode;
import pt.edu.academiafa.Utils.Wind;

import pt.edu.academiafa.pages.Index;

import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.json.JSONCollection;
import org.apache.tapestry5.services.Response;
import org.ros.node.DefaultNodeMainExecutor;
import org.ros.node.NodeMainExecutor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;


/**
 * State page for map updating.
 */
public class State
{
    static TelemetryFuelNode telemetry;
	
	public State() throws InterruptedException {
		  super();	   
	  	  System.out.println("Launching node telemetry listener");
	      NodeMainExecutor executor = DefaultNodeMainExecutor.newDefault();
	      telemetry = new TelemetryFuelNode();
	      executor.execute(telemetry, Index.setupConfiguration());
	      Thread.sleep(200); // to acquire data
	}


	@Inject
    private JsonConverter danielConverter;
    

    /**
     * @return the MSE JSON stream response.
     */
    public StreamResponse onActivate()
    {
        return onActivate(null);
    }

    /**
     * @param what the subset of the MSE to be emitted.
     * @return the MSE JSON stream response.
     */
    public StreamResponse onActivate(final String what)
    {
        return new DanielStreamResponse(danielConverter, what);
    }

    /**
     * MseStreamResponse
     */
    private static class DanielStreamResponse implements StreamResponse
    {
        private JsonConverter danielConverter;
        private String what;
        
        /**
         * @param mseConverter the MSE converter.
         * @param missionStateEstimate the MSE.
         * @param what the subset of the MSE to be emitted.
         */
        public DanielStreamResponse(JsonConverter danielConverter, String what)
        {
            this.danielConverter = danielConverter;
            this.what = what;
        }

        /**
         * {@inheritDoc}
         */
        public String getContentType()
        {
            return "application/json";
        }

        /**
         * {@inheritDoc}
         */
        public InputStream getStream() throws IOException
        {
            JSONCollection m = null;          
            if (what == null)
            {
                m = danielConverter.convertIntegerToJson(-1);
            }
            
            else if("airplane".equals(what))
            {            	
            	Airplane uav = new Airplane(telemetry);
            	m = danielConverter.convertAirplaneToJson(uav);
            }
            
            
            else if ("reachAreaSensor".equals(what))
            {
            	// calculando com o start flight no cliente
            	m = danielConverter.convertDataWindToPolygon(Params.getPolygonPoints(), telemetry.getGS(), 
                		telemetry.getYaw(), telemetry.getFuelLevel(), MissionEstimate.getMissionFuel(),
                		telemetry.getLatitude(), telemetry.getLongitude(), telemetry.getWindSouth(), 
                		telemetry.getWindWest(), telemetry.getElapsedTime());
            }
            else if ("reachAreaAPI".equals(what))
            {
            	m = danielConverter.convertDataWindToPolygon(Params.getPolygonPoints(), telemetry.getGS(), 
                		telemetry.getYaw(), telemetry.getFuelLevel(), MissionEstimate.getMissionFuel(),
                		telemetry.getLatitude(), telemetry.getLongitude(), Wind.getWindSouth(), 
                		Wind.getWindWest(), telemetry.getElapsedTime());
            }
            else if ("reachAreaUser".equals(what))
            {
            	m = danielConverter.convertDataToPolygon(Params.getPolygonPoints(), telemetry.getGS(), 
                		telemetry.getYaw(), telemetry.getFuelLevel(), MissionEstimate.getMissionFuel(),
                		telemetry.getLatitude(), telemetry.getLongitude(), telemetry.getElapsedTime());
            }
            
            
            else if ("missionEstimate".equals(what))
            {            	
            	MissionEstimate mission = new MissionEstimate(telemetry);
            	m = danielConverter.convertMissionEstimateToJSON(mission);
            }
            
            else if ("weather".equals(what))
            {            	
            	m = danielConverter.convertWindsToJSON(Wind.getWinds(telemetry.getLatitude(), telemetry.getLongitude()));
            }
            
            else
            {
                m = danielConverter.convertIntegerToJson(-2);
            }

            ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
            OutputStreamWriter streamWriter = new OutputStreamWriter(out, "UTF-8");
            PrintWriter writer = new PrintWriter(streamWriter);
            m.print(writer);
            writer.close();
            out.close(); 
      
            return new ByteArrayInputStream(out.toByteArray());     
        }

        /**
         * {@inheritDoc}
         */
        public void prepareResponse(final Response response)
        {
            // intentionally empty.
        }
    }
}


//calculando tudo no servidor vai assim:
//ReachArea area = new ReachArea(telemetry);
//m = danielConverter.convertLocationListToJSON(area.getBoundaryPointsSensor());


//calculando tudo no servidor vai assim:
//ReachArea area = new ReachArea();
//m = danielConverter.convertLocationListToJSON(area.getBoundaryPointsAPI());


//calculando tudo no servidor vai assim:
//ReachArea area = new ReachArea();
//m = danielConverter.convertLocationListToJSON(area.getBoundaryPointsUser());
