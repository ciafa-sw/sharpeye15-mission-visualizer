/*
 * This code is part of the BigActor project.
 *
 * Copyright (c) 2013 Clemens Krainer <clemens.krainer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package pt.edu.academiafa.services;


import org.apache.tapestry5.json.JSONCollection;
import org.apache.tapestry5.json.JSONObject;
import org.apache.tapestry5.json.JSONArray;

import pt.edu.academiafa.Utils.Airplane;
//import pt.edu.academiafa.Utils.Location;
import pt.edu.academiafa.Utils.MissionEstimate;
import pt.edu.academiafa.Utils.Params;

import java.text.DecimalFormat;
//import java.util.List;


/**
 * MseConverterImpl
 */
public class JsonConverterImpl implements JsonConverter
{
    /**
     * {@inheritDoc}
     */
    public JSONObject convertIntegerToJson(Integer number)
    {
        JSONObject o = new JSONObject();
        o.put("numero", number);
        return o;
    }



    /**
     * {@inheritDoc}
     */
    /*
    public JSONArray convertLocationListToJSON(List<Location> locations)
    {
        JSONArray a = new JSONArray();
        for (Location l : locations)
        {
            a.put(convertLocationToJSON(l));
        }
        return a;
    }
*/

    
    /**
     * {@inheritDoc}
     */
    /*
    public JSONObject convertLocationToJSON(Location location)
    {
        JSONObject o = new JSONObject();
        o.put("lat", location.getLatitude());
        o.put("lon", location.getLongitude());
        return o;
    }
     */


	public JSONCollection convertAirplaneToJson(Airplane uav) 
	{
		JSONObject o = new JSONObject();
        o.put("lat", uav.getLat());
        o.put("lng", uav.getLng());
        o.put("roll", uav.getRoll());
        o.put("pitch", uav.getPitch());
        o.put("yaw", uav.getYaw());
        
        // Not actually airplane data but parameters for the script
        o.put("centerMapOnAirplane", Params.isCenterMapOnAirplane());
        o.put("centeringPeriod", Params.getCenteringPeriod());
      
        return o;
	}



	public JSONCollection convertMissionEstimateToJSON(MissionEstimate mission)
	{
		JSONObject o = new JSONObject();
        o.put("TAS", mission.getTAS());
        o.put("GS", mission.getGS());
       
        o.put("DistanceCovered", mission.getDistanceCovered());
        o.put("FlightTime", mission.getFlightTime());

        o.put("ETAPortoSanto", mission.getETAOrigin());
        o.put("ETASelvagens", mission.getETADestination());
        
        o.put("TimeLeft", mission.getTimeLeft());
        o.put("Interval", mission.getIntervalTime());
                
        o.put("Fuel", mission.getFuel());
        o.put("EstimatedFuel", mission.getEstimatedFuel());
        
        o.put("WindWest", mission.getWindWest());
        o.put("WindSouth", mission.getWindSouth());
        
        return o;
	}



	public JSONCollection convertWindsToJSON(double[] winds)
	{
		JSONArray a = new JSONArray();
		DecimalFormat df = new DecimalFormat("#.#");   
		int counter = Params.getNumberOfWindHours();
        
		for(int i = 0; i<counter; i=i+1)
		{
			JSONObject wind = new JSONObject();
			wind.put("windSouth", Double.valueOf(df.format(winds[2*i])));
			wind.put("windWest", Double.valueOf(df.format(winds[2*i+1])));
			a.put(wind);
		}
        return a;
	}



	public JSONCollection convertDataToPolygon(int polygonPoints, float gs,
			float yaw, float fuelLevel, double missionFuel, float lat, float lng, int FlightTime)
	{
		JSONObject o = new JSONObject();
        o.put("polygonPoints", polygonPoints);
        o.put("gs", gs);
        o.put("yaw", yaw);
        o.put("fuelLevel", fuelLevel);
        o.put("missionFuel", missionFuel);
        o.put("lat", lat);
        o.put("lng", lng);
        o.put("FlightTime", FlightTime);
		return o;
	}



	public JSONCollection convertDataWindToPolygon(int polygonPoints, float gs, float yaw, float fuelLevel, 
			double missionFuel, float lat, float lng, float windSouth, float windWest, int FlightTime) 
	{
		JSONObject o = new JSONObject();
        o.put("polygonPoints", polygonPoints);
        o.put("gs", gs);
        o.put("yaw", yaw);
        o.put("fuelLevel", fuelLevel);
        o.put("missionFuel", missionFuel);
        o.put("lat", lat);
        o.put("lng", lng);
        o.put("windSouth", windSouth);
        o.put("windWest", windWest);
        o.put("FlightTime", FlightTime);
		return o;
	}


}
