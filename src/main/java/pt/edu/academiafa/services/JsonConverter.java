/*
 * This code is part of the BigActor project.
 *
 * Copyright (c) 2013 Clemens Krainer <clemens.krainer@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package pt.edu.academiafa.services;

import org.apache.tapestry5.json.JSONCollection;
import org.apache.tapestry5.json.JSONObject;
//import java.util.List;
//import org.apache.tapestry5.json.JSONArray;

import pt.edu.academiafa.Utils.Airplane;
//import pt.edu.academiafa.Utils.Location;
import pt.edu.academiafa.Utils.MissionEstimate;



/**
 * ConvertService
 */
public interface JsonConverter
{
    /**
     * @param number to test
     * @return the created JSON object
     */
    JSONObject convertIntegerToJson(Integer number);

	//JSONArray convertLocationListToJSON(List<Location> locations);

	//JSONObject convertLocationToJSON(Location location);

	JSONCollection convertAirplaneToJson(Airplane uav);

	JSONCollection convertMissionEstimateToJSON(MissionEstimate mission);

	JSONCollection convertWindsToJSON(double[] winds);

	JSONCollection convertDataToPolygon(int polygonPoints, float gs, float yaw,
			float fuelLevel, double missionFuel, float f, float g, int flightTime);

	JSONCollection convertDataWindToPolygon(int polygonPoints, float gs, float yaw, float fuelLevel,
			 double missionFuel, float latitude, float longitude, float windSouth, float windWest, int flightTime);

}
