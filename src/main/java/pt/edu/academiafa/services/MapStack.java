
package pt.edu.academiafa.services;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.tapestry5.Asset;
//import org.apache.tapestry5.internal.TapestryInternalUtils;
import org.apache.tapestry5.services.AssetSource;
import org.apache.tapestry5.services.javascript.JavaScriptStack;
import org.apache.tapestry5.services.javascript.StylesheetLink;

	/**
	 * MapStack
	 */
public class MapStack implements JavaScriptStack
	{
	    private StylesheetLink[] stylesheets;

	    private Asset[] javaScriptLibraries;

	    /**
	     * @param assetSource the asset source
	     */
	    public MapStack(final AssetSource assetSource)
	    {
	        stylesheets = new StylesheetLink[]
	            {
	                //TapestryInternalUtils.assetToStylesheetLink.map(assetSource.getUnlocalizedAsset("pt/edu/academiafa/css/map.css")),
	            };
	        javaScriptLibraries = new Asset[]
	            {
	                //assetSource.getUnlocalizedAsset("pt/edu/academiafa/js/jquery-1.9.1.js"),
	                assetSource.getUnlocalizedAsset("pt/edu/academiafa/js/Mapa.js"),
	            };
	    }

	    /**
	     * {@inheritDoc}
	     */
	    public List<String> getStacks()
	    {
	        return Collections.emptyList();
	    }

	    /**
	     * {@inheritDoc}
	     */
	    public List<Asset> getJavaScriptLibraries()
	    {
	        return Arrays.asList(javaScriptLibraries);
	    }

	    /**
	     * {@inheritDoc}
	     */
	    public List<StylesheetLink> getStylesheets()
	    {
	        return Arrays.asList(stylesheets);
	    }

	    /**
	     * {@inheritDoc}
	     */
	    public String getInitialization()
	    {
	        return null;
	    }
	}


/*

package pt.edu.academiafa.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.services.AssetSource;
import org.apache.tapestry5.services.javascript.JavaScriptStack;
import org.apache.tapestry5.services.javascript.StylesheetLink;

public class MapStack implements JavaScriptStack
{		 
	    private AssetSource assetSource;
	 
	    public MapStack(final AssetSource assetSource)
	    {
	        this.assetSource = assetSource;
	    }
	 
	    public String getInitialization()
	    {
	        return null;
	    }
	 
	    public List<Asset> getJavaScriptLibraries()
	    {
	        List<Asset> ret = new ArrayList<Asset>();
	        //ret.add(assetSource.getContextAsset("/pt/edu/academiafa/uavService/js/Hello.js", null));
	        //ret.add(assetSource.getContextAsset("js/Hello.js", null));
	        //ret.add(assetSource.getContextAsset("Hello.js", null));
	        return ret;
	    }
	 
	    public List<StylesheetLink> getStylesheets()
	    {
	        List<StylesheetLink> ret = new ArrayList<StylesheetLink>();
	        //ret.add(new StylesheetLink(assetSource.getContextAsset("static/css/style.css", null)));
	    return ret;
	    }
	 
	    public List<String> getStacks()
	    {
	        return Collections.emptyList();
	    }
	 
	}
*/	