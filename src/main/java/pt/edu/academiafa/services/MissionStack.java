
package pt.edu.academiafa.services;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.tapestry5.Asset;
//import org.apache.tapestry5.internal.TapestryInternalUtils;
import org.apache.tapestry5.services.AssetSource;
import org.apache.tapestry5.services.javascript.JavaScriptStack;
import org.apache.tapestry5.services.javascript.StylesheetLink;

	/**
	 * MapStack
	 */
public class MissionStack implements JavaScriptStack
	{
	    private StylesheetLink[] stylesheets;

	    private Asset[] javaScriptLibraries;

	    /**
	     * @param assetSource the asset source
	     */
	    public MissionStack(final AssetSource assetSource)
	    {
	        stylesheets = new StylesheetLink[]
	            {
	                //TapestryInternalUtils.assetToStylesheetLink.map(assetSource.getUnlocalizedAsset("pt/edu/academiafa/css/map.css")),
	            };
	        javaScriptLibraries = new Asset[]
	            {
	                //assetSource.getUnlocalizedAsset("pt/edu/academiafa/js/jquery-1.9.1.js"),
	                assetSource.getUnlocalizedAsset("pt/edu/academiafa/js/Mission.js"),
	            };
	    }

	    /**
	     * {@inheritDoc}
	     */
	    public List<String> getStacks()
	    {
	        return Collections.emptyList();
	    }

	    /**
	     * {@inheritDoc}
	     */
	    public List<Asset> getJavaScriptLibraries()
	    {
	        return Arrays.asList(javaScriptLibraries);
	    }

	    /**
	     * {@inheritDoc}
	     */
	    public List<StylesheetLink> getStylesheets()
	    {
	        return Arrays.asList(stylesheets);
	    }

	    /**
	     * {@inheritDoc}
	     */
	    public String getInitialization()
	    {
	        return null;
	    }
	}
